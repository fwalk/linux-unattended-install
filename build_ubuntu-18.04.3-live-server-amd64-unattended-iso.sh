#!/bin/bash

set -x

CLEANUP=yes \
    VERSION=18.04.3 FLAVOUR=live-server \
    ./ubuntu-unattended-iso.sh
