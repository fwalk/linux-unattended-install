# Readme

    cat ubuntu_files/isolinux/lang
    en


## References

- http://gyk.lt/ubuntu-16-04-desktop-unattended-installation/
- https://help.ubuntu.com/lts/installation-guide/i386/ch04s06.html
- https://help.ubuntu.com/community/InstallCDCustomization
- https://github.com/the-linux-schools-project/karoshi-server/blob/master/serversetup/modules/distributionserver/tftpboot/preseed/Ubuntu-18.04-preseed.cfg
- https://unix.stackexchange.com/questions/106614/preseed-cfg-ignoring-hostname-setting
- https://www.credativ.de/blog/howtos/howto-debian-preseed-mit-netboot/
- http://www.whiteboardcoder.com/2016/04/install-cloud-init-on-ubuntu-and-use.html