#!/bin/bash -e

VERSION=${VERSION:-18.04.3}
FLAVOUR=${FLAVOUR:-live-server}
ISO=ubuntu-${VERSION}-${FLAVOUR}-amd64.iso
ISO_DEST=ubuntu-${VERSION}-${FLAVOUR}-amd64-unattended.iso
ISO_DEST_VOLUME_NAME=UAI-ubuntu-${VERSION}-${FLAVOUR}
DOWNLOAD_SRC=http://releases.ubuntu.com/${VERSION}/${ISO}
DOWNLOAD_SRC2=http://cdimage.ubuntu.com/releases/${VERSION}/release/${ISO}

USER_ID=$(id -u)
GROUP_ID=$(id -g)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
WORKDIR=${WORKDIR:-$(pwd)}
DISKDIR=${WORKDIR}/disk_mount_${VERSION}-${FLAVOUR}
FILESDIR=${WORKDIR}/disk_files_${VERSION}-${FLAVOUR}

set -o pipefail
set -x

cd $WORKDIR
if [ ! -z "${CLEANUP}" ] || [ ! -z "${CLEANUP_ALL}" ]; then
    [ ! -z "${CLEANUP_ALL}" ] && rm -f $SRC_ISO
    sudo umount $DISKDIR || echo "Ignore: Something happens during umount."
    sudo rm -rf $FILESDIR
    rm -rf $DISKDIR
fi

if [ ! -f $ISO ] ; then 
    if wget --spider $DOWNLOAD_SRC 2>/dev/null; then
        curl --progress-bar --verbose -O $DOWNLOAD_SRC
    else
        curl --progress-bar --verbose -O $DOWNLOAD_SRC2
    fi
fi

if [ ! -d $DISKDIR ] ; then
    mkdir $DISKDIR
    sudo mount -r -o loop $ISO $DISKDIR
fi

if [ ! -d ubuntu_files ] ; then
    mkdir $FILESDIR
    rsync --stats -a $DISKDIR/ $FILESDIR/
    sudo chown ${USER_ID}:${GROUP_ID} $FILESDIR
    sudo chmod 755 $FILESDIR
    sudo umount $DISKDIR
    rm -rf $DISKDIR
fi

sudo chown ${USER_ID}:${GROUP_ID} $FILESDIR/isolinux $FILESDIR/isolinux/{txt.cfg,isolinux.cfg}
chmod 755 $FILESDIR/isolinux
chmod 644 $FILESDIR/isolinux/{txt.cfg,isolinux.cfg}
cp -f ${BASEDIR}/src/preseed.cfg ${FILESDIR}/
cp -f ${BASEDIR}/src/txt.cfg ${FILESDIR}/isolinux/
sed -i 's/timeout.*/timeout 150/' $FILESDIR/isolinux/isolinux.cfg

rm -f ${WORKDIR}/${ISO_DEST}
sudo mkisofs -D -r -V "$ISO_DEST_VOLUME_NAME" -J -l \
    -b isolinux/isolinux.bin \
    -c isolinux/boot.cat \
    -no-emul-boot \
    -boot-load-size 4 \
    -boot-info-table \
    -input-charset utf-8 \
    -cache-inodes \
    -quiet \
    -o $ISO_DEST \
    $FILESDIR/

sudo chown ${USER_ID}:${GROUP_ID} $ISO_DEST
file $ISO_DEST
md5sum -b $ISO_DEST | tee ${ISO_DEST}.md5

